//1
const luas = (panjang, lebar) => {
    return panjang * lebar
}

const keliling = (panjang, lebar) => {
    let x = 2 
    return x * (panjang + lebar)
}

console.log(luas(5,2))
console.log(keliling(5,2))

//2

const newFunction = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName(){
        console.log(this.firstName + " " + this.lastName)
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName() 

//3
const newObject = {
    firstName : "Muhammad",
    lastName : "Iqbal Mubarok",
    address : "Jalan Ranamanyar",
    hobby : "playing football",
}

const {firstName,lastName,address,hobby} = newObject
console.log(firstName,lastName,address,hobby)

//4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west,...east]
//Driver Code
console.log(combined)

//5
const planet = "earth"
const view = " glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
var after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`
console.log(before)
console.log(after)
