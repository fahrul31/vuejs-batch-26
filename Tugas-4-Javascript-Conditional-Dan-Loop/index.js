//1

var nilai = 65;

if(nilai >= 85){
    console.log("Indeks : A\n")
}else if(nilai >= 75 && nilai < 85) {
    console.log("Indeks : B\n")
}else if(nilai >= 65 && nilai < 75) {
    console.log("Indeks : C\n")
}else if(nilai >= 55 && nilai < 65) {
    console.log("Indeks : D\n")
}else if(nilai < 55) {
    console.log("Indeks : E\n")
}

//2
var tanggal = 31;
var bulan = 7;
var tahun = 2021;

switch(bulan){
    case 1 : {console.log(tanggal + ' ' + 'Januari '+ tahun +' \n'); break;}
    case 2 : {console.log(tanggal + ' ' + 'Februari '+ tahun +' \n'); break;}
    case 3 : {console.log(tanggal + ' ' + 'Maret '+ tahun +' \n'); break;}
    case 4 : {console.log(tanggal + ' ' + 'April '+ tahun +' \n'); break;}
    case 5 : {console.log(tanggal + ' ' + 'Mei '+ tahun +' \n'); break;}
    case 6 : {console.log(tanggal + ' ' + 'Juni '+ tahun +' \n'); break;}
    case 7 : {console.log(tanggal + ' ' + 'Juli '+ tahun +' \n'); break;}
    case 8 : {console.log(tanggal + ' ' + 'Agustus '+ tahun +' \n'); break;}
    case 9 : {console.log(tanggal + ' ' + 'September '+ tahun +' \n'); break;}
    case 10 : {console.log(tanggal + ' ' + 'Oktober '+ tahun +' \n'); break;}
    case 11 : {console.log(tanggal + ' ' + 'November '+ tahun +' \n'); break;}
    case 12 : {console.log(tanggal + ' ' + 'Desember '+ tahun +' \n'); break;}
}


//3

var final = '';
for(var i = 0; i < 3; i++){
    for(var s = 0; s <= i; s++){
        final += '#';
    }
    final += '\n';
}
console.log(final)

var final = '';
for(var i = 0; i < 7; i++){
    for(var s = 0; s <= i; s++){
        final += '#';
    }
    final += '\n';
}
console.log(final)

//4
var output = '';
var m = 10;
for(var i = 1 ; i <= m ; i++){
    if( i%3 == 0 ){
        output += i + ' - I love VueJS\n'
        for(var s = 0 ; s < i ; s++){
            output += '='
        }
        output += "\n"
    }else if( i%3 == 1){
        output += i + ' - I love programming\n'
    }else if( i%3 == 2){
        output += i + ' - I love Javascript\n'
    }
}
console.log(output)