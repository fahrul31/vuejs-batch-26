//1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var car1 = pertama.substr(0,4);
var car2 = pertama.substr(12,6);
var car3 = kedua.substr(0,7);
var car4 = kedua.substr(8,10).toUpperCase();

console.log(car1.concat(' ',car2,' ',car3,' ',car4,"\n"));

//2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var number1 = Number(kataPertama);
var number2 = Number(kataKedua);
var number3 = Number(kataKetiga);
var number4 = Number(kataKeempat);

console.log(number1 + number2 * number3 + number4 );

//3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4,14); 
var kataKetiga = kalimat.substring(15,18);  
var kataKeempat = kalimat.substring(19,24); 
var kataKelima = kalimat.substring(25); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);