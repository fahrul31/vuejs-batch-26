//1
var daftarHewan = ["2. Komodo","5. Buaya","3. Cicak","4. Ular","1. Tokek"];

for (var i=0 ; i < daftarHewan.length-1 ; i++){
    min = i;
    for(var j= i+1 ; j < daftarHewan.length ; j++){
        if (daftarHewan[j] < daftarHewan[min]){
            min = j;
        }
    }
    temp = daftarHewan[min];
    daftarHewan[min] = daftarHewan[i];
    daftarHewan[i] = temp;
}

for(var i=0 ; i < daftarHewan.length ; i++){
    console.log(daftarHewan[i]);
}
console.log("\n");

//2
function introduce(data){
    return("Nama saya " + data.name + ',' + ' umur saya ' + data.age + ' tahun, alamat saya di ' + data.address + ', dan saya punya hobby yaitu ' + data.hobby + '!\n')
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

//3
function hitung_huruf_vokal(nama){
    var count = 0;
    nama = nama.toLowerCase();
    var charNama = nama.split('');
    for(var i=0; i < nama.length ; i++){
        if (charNama[i] == 'a' || charNama[i] == 'i' || charNama[i] == 'u' || charNama[i] == 'e' || charNama[i] == 'o'){
            count++;
        }
    }
    return count;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2,'\n')

//4
function hitung (angka, nilai = -2){
    return angka*2 + nilai
}

console.log( hitung(0) )
console.log( hitung(1) ) 
console.log( hitung(2) ) 
console.log( hitung(3) ) 
console.log( hitung(5) )

